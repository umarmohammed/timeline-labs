/*
 * Public API Surface of ngx-d3timeline
 */

export * from './lib/timeline-lib.component';
export * from './lib/ngx-d3timeline.module';
