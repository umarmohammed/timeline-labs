import { ScaleBand, scaleBand } from 'd3-scale';
import { TimelineEvent } from '../timeline-event.model';
import { View } from './view.model';

export class Bandscale {
  readonly bandscale: ScaleBand<string>;

  constructor(data: TimelineEvent[], view: View) {
    this.bandscale = this.createBandscale(data, view);
  }

  map(domain: string): number {
    return this.bandscale(domain);
  }

  tickTransform(tick: string) {
    return `translate(${this.bandscale(tick) +
      this.bandscale.bandwidth() / 2}, 0)`;
  }

  get rangeLimit() {
    return this.bandscale.range()[1];
  }

  get ticks() {
    return this.bandscale.domain();
  }

  get bandwidth() {
    return this.bandscale.bandwidth();
  }

  private createBandscale(data: TimelineEvent[], view: View) {
    return scaleBand()
      .domain([...new Set(data.map(d => d.series))])
      .range([0, view.width - view.margin]);
  }
}
