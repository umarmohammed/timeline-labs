import { TimelineEvent } from '../timeline-event.model';
import { View } from './view.model';
import { Timescale } from './timescale.model';
import { Bandscale } from './bandscale.model';

export class Scales {
  timescale: Timescale;
  bandscale: Bandscale;

  constructor(readonly data: TimelineEvent[], event: any, view: View) {
    this.timescale = new Timescale(data, event, view);
    this.bandscale = new Bandscale(data, view);
  }

  dataTransform(data: TimelineEvent) {
    return `translate(${this.bandscale.map(data.series)}, ${this.timescale.map(
      data.start
    )})`;
  }

  rectHeight(data: TimelineEvent) {
    return this.timescale.map(data.finish) - this.timescale.map(data.start);
  }
}
