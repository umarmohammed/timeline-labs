import { ScaleTime, scaleTime } from 'd3-scale';
import { TimelineEvent } from '../timeline-event.model';
import { View } from './view.model';
import { extent } from 'd3-array';

export class Timescale {
  readonly timescale: ScaleTime<number, number>;

  constructor(data: TimelineEvent[], event: any, view: View) {
    this.timescale = this.createTimescale(data, event, view);
  }

  tickTransform(tick: Date) {
    return `translate(0,${this.timescale(tick)})`;
  }

  map(domain: Date): number {
    return this.timescale(domain);
  }

  get ticks() {
    return this.timescale.ticks();
  }

  tickFormat(tick: Date) {
    return this.timescale.tickFormat()(tick);
  }

  get rangeLimit() {
    return this.timescale.range()[1];
  }

  private createTimescale(data: TimelineEvent[], event: any, view: View) {
    const timescale = scaleTime()
      .domain(extent(data, d => d.start))
      .range([0, view.height - view.margin]);

    return event ? event.transform.rescaleY(timescale) : timescale;
  }
}
