import { NgModule } from '@angular/core';
import { TimelineLibComponent } from './timeline-lib.component';
import { TimelineBarComponent } from './timeline-bar.component';
import { BandScaleComponent } from './band-scale.component';
import { TimeScaleComponent } from './time-scale.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    TimelineLibComponent,
    TimelineBarComponent,
    BandScaleComponent,
    TimeScaleComponent
  ],
  imports: [CommonModule],
  exports: [
    TimelineLibComponent,
    TimelineBarComponent,
    BandScaleComponent,
    TimeScaleComponent
  ]
})
export class NgxD3timelineModule {}
