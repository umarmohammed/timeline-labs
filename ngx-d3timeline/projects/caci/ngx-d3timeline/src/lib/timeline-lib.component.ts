import {
  Component,
  AfterViewInit,
  ViewChild,
  ElementRef,
  Input
} from '@angular/core';
import { ScaleService } from './scale.service';
import { zoom } from 'd3-zoom';
import { select, event } from 'd3-selection';
import { TimelineEvent } from './timeline-event.model';

@Component({
  selector: 'lib-app-timeline',
  template: `
    <svg
      #svgEl
      [attr.width]="scaleService.view.width"
      [attr.height]="scaleService.view.height"
    >
      <g [attr.transform]="scaleService.rootTransform">
        <g lib-app-band-scale [scale]="scaleService.bandscale$ | async"></g>
        <g lib-app-time-scale [scale]="scaleService.timescale$ | async"></g>
        <g lib-app-timeline-bar [scales]="scaleService.scales$ | async"></g>
      </g>
    </svg>
  `
})
export class TimelineLibComponent implements AfterViewInit {
  @Input() set data(value: TimelineEvent[]) {
    this.scaleService.setData(value);
  }

  @ViewChild('svgEl', { static: false })
  svgEl: ElementRef;

  constructor(public scaleService: ScaleService) {}

  ngAfterViewInit(): void {
    const onZoom = zoom().on('zoom', () => this.scaleService.onEvent(event));
    onZoom(select(this.svgEl.nativeElement));
  }
}
