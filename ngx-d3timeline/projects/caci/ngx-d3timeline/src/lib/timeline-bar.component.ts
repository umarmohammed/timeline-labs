import { Component, Input } from '@angular/core';
import { Scales } from './model/scales.model';

@Component({
  selector: 'g[lib-app-timeline-bar]',
  template: `
    <svg:g
      [attr.transform]="scales.dataTransform(d)"
      *ngFor="let d of scales.data"
    >
      <rect
        [attr.height]="scales.rectHeight(d)"
        [attr.width]="scales.bandscale.bandwidth"
        fill="none"
        stroke="#000"
      ></rect>
      <text font-size="10px" dy="1em">{{ d.type }}</text>
    </svg:g>
  `
})
export class TimelineBarComponent {
  @Input() scales: Scales;
}
