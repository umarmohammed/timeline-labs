import { Component, Input } from '@angular/core';
import { ScaleTime } from 'd3-scale';
import { Timescale } from './model/timescale.model';

@Component({
  selector: 'g[lib-app-time-scale]',
  template: `
    <svg:g
      *ngFor="let tick of scale.ticks"
      [attr.transform]="scale.tickTransform(tick)"
    >
      <svg:text
        font-size="10px"
        text-anchor="end"
        dx="-7"
        dominant-baseline="central"
      >
        {{ scale.tickFormat(tick) }}
      </svg:text>
      <svg:line x2="-5" stroke="#000"></svg:line>
    </svg:g>
    <svg:line [attr.y2]="scale.rangeLimit" stroke="#000"></svg:line>
  `
})
export class TimeScaleComponent {
  @Input() scale: Timescale;
}
