import { TestBed, ComponentFixture } from '@angular/core/testing';
import { BandScaleComponent } from './band-scale.component';
import { View } from './model/view.model';
import { Bandscale } from './model/bandscale.model';
import { data } from './data';

describe('BandscaleComponent', () => {
  let fixture: ComponentFixture<BandScaleComponent>;
  let scale: Bandscale;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BandScaleComponent]
    });

    fixture = TestBed.createComponent(BandScaleComponent);

    const view: View = { width: 600, height: 800, margin: 50 };
    scale = new Bandscale(data, view);
  });

  it('should have correct number of ticks', () => {
    fixture.componentInstance.scale = scale;

    fixture.detectChanges();

    expect(fixture.nativeElement.querySelectorAll('g').length).toEqual(3);
  });

  it('should render', () => {
    fixture.componentInstance.scale = scale;

    fixture.detectChanges();

    expect(fixture.nativeElement).toMatchSnapshot();
  });
});
