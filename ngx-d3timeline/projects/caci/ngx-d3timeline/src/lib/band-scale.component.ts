import { Component, Input } from '@angular/core';
import { Bandscale } from './model/bandscale.model';

@Component({
  selector: 'g[lib-app-band-scale]',
  template: `
    <svg:g
      *ngFor="let tick of scale.ticks"
      [attr.transform]="scale.tickTransform(tick)"
    >
      <text font-size="12px" text-anchor="middle" dy="-2">
        {{ tick }}
      </text>
    </svg:g>
    <svg:line [attr.x2]="scale.rangeLimit" stroke="#000"></svg:line>
  `
})
export class BandScaleComponent {
  @Input() scale: Bandscale;
}
