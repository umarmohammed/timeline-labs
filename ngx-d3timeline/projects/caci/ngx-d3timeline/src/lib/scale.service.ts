import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { switchMap, map, share, pluck } from 'rxjs/operators';
import { TimelineEvent } from './timeline-event.model';
import { View } from './model/view.model';
import { Scales } from './model/scales.model';

@Injectable({ providedIn: 'root' })
export class ScaleService {
  view: View = { width: 600, height: 800, margin: 50 };

  private eventSubject = new BehaviorSubject<any>(null);
  event$ = this.eventSubject.asObservable();

  private dataSubject = new BehaviorSubject<TimelineEvent[]>(null);

  scales$ = this.dataSubject.pipe(
    switchMap(data =>
      this.event$.pipe(
        map(event => new Scales(data, event, this.view)),
        share()
      )
    )
  );

  timescale$ = this.scales$.pipe(pluck('timescale'));
  bandscale$ = this.scales$.pipe(pluck('bandscale'));

  setData(data: TimelineEvent[]) {
    this.dataSubject.next(data);
  }

  onEvent(event: any) {
    this.eventSubject.next(event);
  }

  get rootTransform() {
    return `translate(${this.view.margin}, ${this.view.margin})`;
  }
}
