import { Component } from '@angular/core';

@Component({
  selector: 'app-bar',
  template: `
    <svg>
      <rect
        *ngFor="let d of data; index as i"
        [attr.width]="d * 100"
        [attr.height]="100"
        [attr.y]="i * 150"
      ></rect>
    </svg>
  `,
  styles: [
    `
      svg {
        width: 1000px;
        height: 800px;
      }
    `
  ]
})
export class BarsComponent {
  data = [5, 3, 1, 4];
}
