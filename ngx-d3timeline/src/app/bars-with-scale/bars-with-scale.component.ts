import { Component, OnInit } from '@angular/core';
import { scaleLinear, scaleBand, ScaleBand, ScaleLinear } from 'd3-scale';

@Component({
  selector: 'app-bars-with-scale',
  template: `
    <svg [attr.width]="width" [attr.height]="height">
      <g [attr.transform]="rootTransform">
        <g>
          <rect
            *ngFor="let d of data"
            [attr.width]="x(d.value)"
            [attr.height]="y.bandwidth()"
            [attr.y]="y(d.name)"
            fill="#6a0"
            stroke="#f00"
          ></rect>
        </g>
        <g app-x-scale [width]="width" [margin]="margin" [scale]="x"></g>
        <g app-y-scale [scale]="y"></g>
      </g>
    </svg>
  `
})
export class BarsWithScaleComponent implements OnInit {
  data = [
    { value: 5, name: 'foo' },
    { value: 3, name: 'bar' },
    { value: 1, name: 'baz' },
    { value: 4, name: 'fred' },
    { value: 11, name: 'foobar' }
  ];

  width = 600;
  height = 500;
  margin = 50;
  x: ScaleLinear<number, number>;
  y: ScaleBand<string>;

  ngOnInit(): void {
    this.x = scaleLinear()
      .domain([0, Math.max(...this.data.map(d => d.value))])
      .range([0, this.width - 2 * this.margin]);

    this.y = scaleBand()
      .domain(this.data.map(d => d.name))
      .range([0, this.height - this.margin]);
  }

  get rootTransform(): string {
    return `translate(${this.margin}, ${this.margin})`;
  }
}
