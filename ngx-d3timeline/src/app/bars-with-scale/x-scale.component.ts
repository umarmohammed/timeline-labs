import { Component, Input, OnInit } from '@angular/core';
import { ScaleLinear } from 'd3-scale';

// Note the use of the selector g[app-x-scale]
// This is so we can use this component within
// an svg without creating an invalid svg element
@Component({
  selector: 'g[app-x-scale]',
  template: `
    <!-- we prefix the svg: namespace in front of svg elements 
    to signify that this markup will be used inside and svg -->
    <svg:line [attr.x2]="width - 2 * margin + 1" stroke="#000"></svg:line>
    <svg:g
      *ngFor="let tick of scale.ticks()"
      [attr.transform]="tickTransform(tick)"
    >
      <svg:text y="-9" font-size="10px" text-anchor="middle">
        {{ tick }}
      </svg:text>
      <svg:line y2="-6" stroke="#000"></svg:line>
    </svg:g>
  `
})
export class XScaleComponent {
  @Input() width: number;
  @Input() margin: number;
  @Input() scale: ScaleLinear<number, number>;

  tickTransform(tick: number): string {
    return `translate(${this.scale(tick)}, 0)`;
  }
}
