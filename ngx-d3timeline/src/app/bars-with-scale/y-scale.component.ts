import { Component, Input } from '@angular/core';
import { ScaleBand } from 'd3-scale';

// Note the use of the selector g[app-x-scale]
// This is so we can use this component within
// an svg without creating an invalid svg element
@Component({
  selector: 'g[app-y-scale]',
  template: `
    <!-- we prefix the svg: namespace in front of svg elements 
    to signify that this markup will be used inside and svg -->
    <svg:text
      *ngFor="let tick of scale.domain()"
      [attr.transform]="tickTransform(tick)"
      text-anchor="end"
      dx="-.5em"
    >
      {{ tick }}
    </svg:text>
  `
})
export class YScaleComponent {
  @Input() scale: ScaleBand<string>;

  tickTransform(tick: string) {
    return `translate(0,${this.scale(tick) + this.scale.bandwidth() / 2})`;
  }
}
