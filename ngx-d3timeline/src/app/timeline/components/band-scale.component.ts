import { Component, Input } from '@angular/core';
import { ScaleBand } from 'd3-scale';

@Component({
  selector: 'g[app-band-scale]',
  template: `
    <svg:g
      *ngFor="let tick of scale.domain()"
      [attr.transform]="tickTransform(tick)"
    >
      <text font-size="10px" text-anchor="middle" dy="-2">
        {{ tick }}
      </text>
    </svg:g>
    <svg:line [attr.x2]="scale.range()[1]" stroke="#000"></svg:line>
  `
})
export class BandScaleComponent {
  @Input() scale: ScaleBand<string>;

  tickTransform(tick: string) {
    return `translate(${this.scale(tick) + this.scale.bandwidth() / 2}, 0)`;
  }
}
