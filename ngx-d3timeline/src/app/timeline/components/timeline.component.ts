import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import { ScaleBand, scaleBand, ScaleTime, scaleTime } from 'd3-scale';
import { data } from '../data';
import { extent } from 'd3-array';
import { ScaleService } from '../scale.service';
import { zoom } from 'd3-zoom';
import { select, event } from 'd3-selection';

@Component({
  selector: 'app-timeline',
  template: `
    <svg #svgEl [attr.width]="width" [attr.height]="height">
      <g [attr.transform]="rootTransform">
        <g app-band-scale [scale]="bandScale"></g>
        <g app-time-scale [scale]="scaleService.timescale$ | async"></g>
        <g
          app-timeline-bar
          [bandScale]="bandScale"
          [timeScale]="scaleService.timescale$ | async"
          [data]="data"
        ></g>
      </g>
    </svg>
  `
})
export class TimelineComponent implements OnInit, AfterViewInit {
  width = 600;
  height = 800;
  margin = 50;
  data = data;

  bandScale: ScaleBand<string>;
  timeScale: ScaleTime<number, number>;

  @ViewChild('svgEl', { static: false })
  svgEl: ElementRef;

  constructor(public scaleService: ScaleService) {}

  ngOnInit(): void {
    this.bandScale = scaleBand()
      .domain([...new Set(this.data.map(d => d.series))])
      .range([0, this.width - this.margin]);

    this.scaleService.setTimeScale(
      scaleTime()
        .domain(extent(this.data, d => d.start))
        .range([0, this.height - this.margin])
    );
  }

  ngAfterViewInit(): void {
    const onZoom = zoom().on('zoom', () => this.scaleService.onEvent(event));
    onZoom(select(this.svgEl.nativeElement));
  }

  get rootTransform() {
    return `translate(${this.margin}, ${this.margin})`;
  }
}
