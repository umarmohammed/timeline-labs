import { Component, Input } from '@angular/core';
import { ScaleBand, ScaleTime } from 'd3-scale';
import { TimelineEvent } from '../timeline-event.model';

@Component({
  selector: 'g[app-timeline-bar]',
  template: `
    <svg:g [attr.transform]="dataTransform(d)" *ngFor="let d of data">
      <rect
        [attr.height]="rectHeight(d)"
        [attr.width]="bandScale.bandwidth()"
        fill="none"
        stroke="#000"
      ></rect>
      <text font-size="10px" dy="1em">{{ d.type }}</text>
    </svg:g>
  `
})
export class TimelineBarComponent {
  @Input() bandScale: ScaleBand<string>;
  @Input() timeScale: ScaleTime<number, number>;
  @Input() data: TimelineEvent[];

  dataTransform(data: TimelineEvent) {
    return `translate(${this.bandScale(data.series)}, ${this.timeScale(
      data.start
    )})`;
  }

  rectHeight(data: TimelineEvent) {
    return this.timeScale(data.finish) - this.timeScale(data.start);
  }
}
