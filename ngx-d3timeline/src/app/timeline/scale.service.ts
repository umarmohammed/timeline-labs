import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ScaleTime } from 'd3-scale';
import { switchMap, map, share } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ScaleService {
  private eventSubject = new BehaviorSubject<any>(null);
  event$ = this.eventSubject.asObservable();

  private timescaleSubject = new BehaviorSubject<ScaleTime<number, number>>(
    null
  );

  timescale$ = this.timescaleSubject.asObservable().pipe(
    switchMap(timeScale =>
      this.event$.pipe(
        map(event => (event ? event.transform.rescaleY(timeScale) : timeScale)),
        share()
      )
    )
  );

  setTimeScale(timeScale: ScaleTime<number, number>) {
    this.timescaleSubject.next(timeScale);
  }

  onEvent(event: any) {
    this.eventSubject.next(event);
  }
}
