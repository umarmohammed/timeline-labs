import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HelloWorldComponent } from './hello-world.component';
import { BarsComponent } from './bars.component';
import { BarsWithScaleComponent } from './bars-with-scale/bars-with-scale.component';
import { XScaleComponent } from './bars-with-scale/x-scale.component';
import { YScaleComponent } from './bars-with-scale/y-scale.component';
import { TimelineComponent } from './timeline/components/timeline.component';
import { BandScaleComponent } from './timeline/components/band-scale.component';
import { TimeScaleComponent } from './timeline/components/time-scale.component';
import { TimelineBarComponent } from './timeline/components/timeline-bar.component';
import {
  NgxD3timelineModule,
  TimelineLibComponent
} from '@caci/ngx-d3timeline';
import { TimelineLibWrapperComponent } from './timeline-lib-wrapper.component';

const routes: Routes = [
  { path: 'hello-world', component: HelloWorldComponent },
  { path: 'bars', component: BarsComponent },
  { path: 'bars-with-scale', component: BarsWithScaleComponent },
  { path: 'timeline', component: TimelineComponent },
  { path: 'caci-timeline', component: TimelineLibWrapperComponent },
  { path: '', component: HelloWorldComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    BarsComponent,
    BarsWithScaleComponent,
    XScaleComponent,
    YScaleComponent,
    TimelineComponent,
    BandScaleComponent,
    TimeScaleComponent,
    TimelineBarComponent,
    TimelineLibWrapperComponent
  ],
  imports: [BrowserModule, RouterModule.forRoot(routes), NgxD3timelineModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
