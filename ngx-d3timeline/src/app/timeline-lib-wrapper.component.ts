import { Component } from '@angular/core';
import { data } from './timeline/data';

@Component({
  selector: 'app-timeline-lib-wrapper',
  template: `
    <lib-app-timeline [data]="data"></lib-app-timeline>
  `
})
export class TimelineLibWrapperComponent {
  data = data;
}
