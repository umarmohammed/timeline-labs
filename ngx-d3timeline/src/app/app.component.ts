import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <a [routerLink]="['hello-world']">Hello world</a> |
    <a [routerLink]="['bars']">Bars</a> |
    <a [routerLink]="['bars-with-scale']">Bars With Scale</a> |
    <a [routerLink]="['timeline']">Timeline</a> |
    <a [routerLink]="['caci-timeline']">CACI Timeline</a>
    <div>
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent {}
