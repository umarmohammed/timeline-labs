This repository contains the code which accompanies the d3/angular sessions. The purpose of these sessions is to cover the prerequisites for creating a timeline control in Angular.

## Code

- d3-getting-started: contains code from the first session where we went from creating simple SVGs by hand to using d3 to create an interactive timeline with axes.

## Resources

- SVG: https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Adding_vector_graphics_to_the_Web
- D3: https://github.com/d3/d3/wiki
